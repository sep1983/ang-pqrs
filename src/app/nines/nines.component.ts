import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators} from '@angular/forms';
import { LoadFormService } from '../load-form.service';

@Component({
  selector: 'app-nines',
  templateUrl: './nines.component.html',
  styleUrls: ['./nines.component.css'],
  providers: [ LoadFormService ]
})

export class NinesComponent implements OnInit {

    siteKey:string = '6LeTNCAbAAAAAGTdff3GXmiulSI32AGRx1AZkzL3';
    showDiv:boolean = false;

    files: File[] = [];
    dataSelect:any[];
    divEjet:any[];
    divCont:any[];
    divPais:any[];
    divDept:any[];
    divMuni:any[];

    divPaisRe:any[];
    divDepaRe:any[];

    form: FormGroup = new FormGroup({
        tipoDomc:  new FormControl({value:""}, [Validators.required]),
        genero:    new FormControl({value:""}, [Validators.required]),
        numIdent:  new FormControl('', [
            Validators.required,
            Validators.pattern("^[0-9]*$")]),
        tipoSolc:  new FormControl({value:""}, [Validators.required]),
        nombApell: new FormControl('', [Validators.required]),
        tipoDisca: new FormControl({value:""}, [Validators.required]),
        estraSoci: new FormControl({value:""}, [Validators.required]),
        atencPref: new FormControl({value:""}, [Validators.required]),
        rangoEdad: new FormControl({value:""}, [Validators.required]),
        condiLing: new FormControl({value:""}, [Validators.required]),
        inforPobl: new FormControl({value:""}, [Validators.required]),
        grupoEtni: new FormControl({value:""}, [Validators.required]),
        telffijo:  new FormControl('', [ Validators.required]),
        telfmovi:  new FormControl('', [
            Validators.required,
            Validators.pattern("^[0-9]*$")]),
        direccio:  new FormControl('', [Validators.required]),
        paisName:  new FormControl({value:""}, [Validators.required]),
        deptName:  new FormControl({value:"", disabled: true}, [Validators.required]),
        muniName:  new FormControl({value:"", disabled: true}, [Validators.required]),
        ejeName:   new FormControl({value:""}, [Validators.required]),
        asunto:    new FormControl('', [
            Validators.required,
            Validators.minLength(5),
            Validators.maxLength(120)
        ]),
        descripn: new FormControl('', [
            Validators.required,
            Validators.minLength(5),
            Validators.maxLength(120)
        ]),
        corrElec:  new FormControl('', [Validators.required, Validators.email]),
        recaptcha: new FormControl('', [Validators.required]),
    });

    constructor(private loadformService: LoadFormService ){

        this.loadformService.dataSelect().then(
            (response:any) => {
                this.dataSelect = response.dataSelect;
                this.divCont = response.continente;
                this.divPais = response.pais;
                this.divEjet = response.ejeTematico;

                let pais:any = [];
                let data:any = [];

                pais = this.divPais.forEach((element:any) => {
                    let obj = { id_pais: element.id_pais,
                        nombre_pais: element.nombre_pais
                    };

                    if(data.filter((e:any) => (e.id_pais === obj.id_pais
                                               && e.nombre_pais === obj.nombre_pais)).length === 0){
                                                   data.push(obj);
                                               }
                });

                this.form.controls['paisName'].enable();
                this.divPais = data;

                this.form.controls['deptName'].disable();
                this.form.controls['muniName'].disable();
                this.divDept = [];

                this.form.controls['paisName'].setValue({value:"170"});
            }
        );
    }

    ngOnInit() : void {}

    get f(){
        return this.form.controls;
    }

    onChangePais($event:any) {
        const element = event.currentTarget as HTMLInputElement
        const value = element.value
        let dept:any = [];
        let data:any = [];

        if(value == '170'){
            this.showDiv = true;
        }else{
            this.showDiv = false;
        }

        if(value != '0'){

            dept = this.dataSelect.filter( (sedata:any, index:any) => {
                return sedata.id_pais == value;
            });

            this.divDepaRe = dept;

            dept.forEach((element:any) => {
                let obj = {
                    id_departamento: element.id_departamento,
                    nombre_departamento: element.nombre_departamento
                };

                if(data.filter((e:any) => (e.id_departamento === obj.id_departamento
                                           && e.nombre_departamento === obj.nombre_departamento)).length === 0){
                                               data.push(obj);
                                           }
            });

            this.form.controls['deptName'].enable();
            this.divDept = data;
        }else{
            this.divDept = [];
            this.form.controls['deptName'].disable();
        }

        this.form.controls['muniName'].disable();
        this.divMuni = [];
    }

    onChangeDept($event:any) {
        const element = event.currentTarget as HTMLInputElement
        const value = element.value
        let munc:any = [];
        let data:any = [];

        if(value != '0'){

            munc = this.divDepaRe.filter( (sedata:any, index:any) => {
                return sedata.id_departamento == value;
            })


            munc.forEach((element:any) => {
                let obj = {
                    id_municipio: element.id_municipio,
                    nombre_municipio: element.nombre_municipio
                };

                if(data.filter((e:any) => (e.id_municipio === obj.id_municipio
                                           && e.nombre_municipio === obj.nombre_municipio)).length === 0){
                                               data.push(obj);
                                           }
            });

            this.form.controls['muniName'].enable();
            this.divMuni = data;

        }else{
            this.divDept = [];
            this.form.controls['muniName'].disable();
        }
    }

    onChangeMunc($event:any){

    }

    onChangeEje($event:any){

    }

    submit(){
        if(this.form.status === 'VALID'){
            console.log(this.form.value);
        }
    }
}

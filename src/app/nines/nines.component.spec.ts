import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NinesComponent } from './nines.component';

describe('NinesComponent', () => {
  let component: NinesComponent;
  let fixture: ComponentFixture<NinesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NinesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NinesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

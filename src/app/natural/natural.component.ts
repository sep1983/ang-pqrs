import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators} from '@angular/forms';
import { LoadFormService } from '../load-form.service';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-natural',
  templateUrl: './natural.component.html',
  styleUrls: ['./natural.component.css'],
  providers: [ LoadFormService ]
})

export class NaturalComponent implements OnInit {

    showDiv:boolean = false;
    defaultValue = '';
    defaultValGe = '';
    defaultValdi = '';
    defaultValli = '';
    defaultValso = '';
    defaultValPo = '';
    tiempos = 0;

    files: File[] = [];
    dataSelect:any[];
    divEjet:any[];
    divCont:any[];
    divPais:any[];
    divDept:any[];
    divMuni:any[];
    divTipoDoc:any[];
    divGen:any[];
    divDisc:any[];
    divLing:any[];
    divEst:any[];
    divInfPo:any[];
    divRanEd1:any[];
    divRanEd2:any[];
    divGruEsp:any[];
    divAtenPr:any[];
    divTipPet:any[];
    divTipSol:any[];

    divPaisRe:any[];
    divDepaRe:any[];

    form: FormGroup = new FormGroup({
        tipoDomc:  new FormControl(''            , [Validators.required])              ,
        numIdent:  new FormControl(''            , [Validators.required                , Validators.pattern("^[0-9]*$"), Validators.minLength(5)])     ,
        nombApell: new FormControl(''            , [Validators.required                , Validators.pattern("^[a-zA-Z ]*$"), Validators.minLength(8)]) ,
        tipoDisca: new FormControl(''            , [Validators.required])              ,
        estraSoci: new FormControl('')           ,
        rangoEdad: new FormControl('')           ,
        atencPref: new FormControl({value:""}    , [Validators.required])              ,
        genero:    new FormControl({value:""}    , [Validators.required])              ,
        condiLing: new FormControl(''            , [Validators.required])              ,
        inforPobl: new FormControl('')           ,
        grupoEtni: new FormControl('')           ,
        telffijo:  new FormControl('', [ Validators.minLength(7)])           ,
        corrElec:  new FormControl(''            , [Validators.required                , Validators.email])                   ,
        tipAutori: new FormControl('Electrónico' , [Validators.required])              ,
        telfmovi:  new FormControl(''            , [  Validators.pattern("^[0-9]*$")   , Validators.minLength(10)]),
        adjuntos:  new FormControl('')           ,
        direccio:  new FormControl('')           ,
        paisName:  new FormControl(''            , [Validators.required])              ,
        deptName:  new FormControl({value:""     , disabled: true}                     , [Validators.required])               ,
        muniName:  new FormControl({value:""     , disabled: true}                     , [Validators.required])               ,
        tipPetic:  new FormControl(''            , [Validators.required])              ,
        tipSolic:  new FormControl(''            , [Validators.required])              ,
        asunto:    new FormControl(''            , [ Validators.required               , Validators.minLength(5)              , Validators.maxLength(120) ]) ,
        descripn:  new FormControl(''            , [ Validators.required               , Validators.minLength(5)              , Validators.maxLength(250) ]) ,
    });

    constructor(public http: HttpClient, private router: Router, private loadformService: LoadFormService ){

        this.loadformService.dataSelect().then(
            (response:any) => {
                this.dataSelect = response.divpola;
                this.divCont    = response.continente;
                this.divPais    = response.pais;
                this.divEjet    = response.ejeTematico;
                this.divGen     = response.genero;
                this.divDisc    = response.discapacidad;
                this.divTipoDoc = response.tipoDoc;
                this.divLing    = response.linguistica;
                this.divEst     = response.estrato;
                this.divInfPo   = response.infPoblacional;
                this.divRanEd1  = response.rangoEdad1;
                this.divRanEd2  = response.rangoEdad2;
                this.divGruEsp  = response.grupoEspecial;
                this.divAtenPr  = response.atenPreferencial;
                this.divTipPet  = response.tipoDePeticion;
                this.divTipSol  = response.tipoDeSolicitud;

                let pais:any = [];
                let data:any = [];

                pais = this.divPais.forEach((element:any) => {
                    let obj = { id_pais: element.id_pais,
                        nombre_pais: element.nombre_pais
                    };

                    if(data.filter((e:any) => (e.id_pais === obj.id_pais
                                               && e.nombre_pais === obj.nombre_pais)).length === 0){
                                                   data.push(obj);
                                               }
                });

                this.form.controls['paisName'].enable();
                this.divPais = data;

                this.form.controls['deptName'].disable();
                this.form.controls['muniName'].disable();
                this.divDept = [];
            }
        );
    }

    ngOnInit() : void {}

    get f(){
        return this.form.controls;
    }

    keepOrder = (a:any, b:any) => {
        return a;
    }

    onChangePais($event:any) {
        const element = event.currentTarget as HTMLInputElement
        const value = element.value
        let dept:any = [];
        let data:any = [];

        if(value == '170'){
            this.showDiv = true;
        }else{
            this.showDiv = false;
        }

        if(value != '0'){

            dept = this.dataSelect.filter( (sedata:any, index:any) => {
                return sedata.id_pais == value;
            });

            this.divDepaRe = dept;


            dept.forEach((element:any) => {
                let obj = {
                    id_departamento: element.id_departamento,
                    nombre_departamento: element.nombre_departamento
                };

                if(data.filter((e:any) => (e.id_departamento === obj.id_departamento
                                           && e.nombre_departamento === obj.nombre_departamento)).length === 0){
                                               data.push(obj);
                                           }
            });

            this.form.controls['deptName'].enable();

            this.divDept = data.sort(function (a:any, b:any) {

                var nameA = a.nombre_departamento.toUpperCase(); // ignore upper and lowercase
                var nameB = b.nombre_departamento.toUpperCase(); // ignore upper and lowercase

                if (nameA < nameB) {
                    return -1; //nameA comes first
                }

                if (nameA > nameB) {
                    return 1; // nameB comes first
                }

                return 0;  // names must be equal
            });

        }else{
            this.divDept = [];
            this.form.controls['deptName'].disable();
        }

        this.form.controls['muniName'].disable();
        this.divMuni = [];
    }

    onChangeDept($event:any) {
        const element = event.currentTarget as HTMLInputElement
        const value = element.value
        let munc:any = [];
        let data:any = [];

        if(value != '0'){

            munc = this.divDepaRe.filter( (sedata:any, index:any) => {
                return sedata.id_departamento == value;
            })


            munc.forEach((element:any) => {
                let obj = {
                    id_municipio: element.id_municipio,
                    nombre_municipio: element.nombre_municipio
                };

                if(data.filter((e:any) => (e.id_municipio === obj.id_municipio
                                           && e.nombre_municipio === obj.nombre_municipio)).length === 0){
                                               data.push(obj);
                                           }
            });

            this.form.controls['muniName'].enable();
            this.divMuni = data;

        }else{
            this.divDept = [];
            this.form.controls['muniName'].disable();
        }
    }

    onSelect(event:any) {
        this.files.push(...event.addedFiles);
    }

    onRemove(event:any) {
        this.files.splice(this.files.indexOf(event), 1);
    }

    onChangeMunc($event:any){

    }

    onChangeEje($event:any){

    }


    onChangeSolc($event:any){
        switch (Number($event)) {
            case 181:
            case 182:
                this.tiempos = 10;
            break;
            case 183:
                this.tiempos = 30;
            break;
            case 189:
                this.tiempos = 5;
            break;
            default:
                this.tiempos = 15;
            break;
        }

    }

    submitFunc() {
        if(this.form.status === 'VALID'){

            this.loadformService.submitForm(this.form.value)
            .subscribe(
                (data) => {

                const formData = new FormData();

                for (let i = 0; i < this.files.length; i++) {
                    formData.append(this.files[i].name, this.files[i])
                }

                formData.append('radicado', JSON.stringify(data));

                this.loadformService.submitAnex(formData).subscribe(
                    (color) => {
                        this.router.navigateByUrl('radicado/' + data['radicado'] + '/' + data['verificacion']);
                    },

                    (error: HttpErrorResponse) => {
                        console.log(error);
                    }
                );

                },
                (error: HttpErrorResponse) => {
                    console.log(error);
                }
            );
        }
    }

}

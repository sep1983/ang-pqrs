import { NgModule, APP_INITIALIZER } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';

import { NgxDropzoneModule } from 'ngx-dropzone';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxCaptchaModule } from 'ngx-captcha';
import { AnonimaComponent } from './anonima/anonima.component';
import { NinesComponent } from './nines/nines.component';
import { HttpClientModule } from '@angular/common/http';

import { LoadFormService } from './load-form.service';
import { PersonalComponent } from './personal/personal.component';
import { JuridicaComponent } from './juridica/juridica.component';
import { ApoderadoComponent } from './apoderado/apoderado.component';
import { NaturalComponent } from './natural/natural.component';
import { ResultadoComponent } from './resultado/resultado.component';
import { AppRoutingModule } from './app-routing.module';
import { PqrsdComponent } from './pqrsd/pqrsd.component';


export function init_app(loadformservice: LoadFormService) {
    return () => loadformservice.dataSelect();
}

@NgModule({

  declarations: [
    AppComponent,
    AnonimaComponent,
    NinesComponent,
    PersonalComponent,
    JuridicaComponent,
    ApoderadoComponent,
    NaturalComponent,
    ResultadoComponent,
    PqrsdComponent
  ],

  imports: [
      BrowserModule,
      ReactiveFormsModule,
      FormsModule,
      NgxCaptchaModule,
      HttpClientModule,
      NgxDropzoneModule,
      AppRoutingModule
  ],

  providers: [ LoadFormService,
      {
          provide: APP_INITIALIZER,
          useFactory: init_app,
          deps: [LoadFormService],
          multi: true
      }
  ],

  bootstrap: [ AppComponent ]

})

export class AppModule { }

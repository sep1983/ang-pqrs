import { Injectable, isDevMode} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { environment } from '../environments/environment'

@Injectable({
    providedIn: 'root'
})

export class LoadFormService {

    arrPola:any[] = [];
    arrNat:any[] = [];
    private radUrl:string;
    private creatRadUrl:string;
    private creatAnxUrl:string;

    constructor(private httpClient: HttpClient) {
        this.radUrl = environment.radUrl;
        this.creatRadUrl = environment.creatRadUrl;
        this.creatAnxUrl = environment.creatAnxUrl;
    }

    getNaturalez(){
        this.arrNat =
            [
            {
                "id_naturaleza": "0",
                "nombre_naturaleza": "Privado o Mixta",
                "id_sector": "0",
                "nombre_sector": "Agropecuario",
                "id_grupo": "0",
                "grupo_nombre": "--",
                "tooltip_sector": "Dentro de este sector se encuentran la agricultura, la ganadería, la silvicultura, la caza y la pesca."
            },
            {
                "id_naturaleza": "0",
                "nombre_naturaleza": "Privado o Mixta",
                "id_sector": "1",
                "nombre_sector": "Servicios",
                "id_grupo": "0",
                "grupo_nombre": "--",
                "tooltip_sector": "Como ejemplos de ello tenemos el comercio, los restaurantes, los hoteles, el transporte, los servicios financieros, las comunicaciones, los servicios de educación, los servicios profesionales, el Gobierno, etc."
            },
            {
                "id_naturaleza": "0",
                "nombre_naturaleza": "Privado o Mixta",
                "id_sector": "2",
                "nombre_sector": "Industrial",
                "id_grupo": "0",
                "grupo_nombre": "--",
                "tooltip_sector": "Comprende todas las actividades económicas de un país relacionadas con la transformación industrial de los alimentos y otros tipos de bienes o mercancías, los cuales se utilizan como base para la fabricación de nuevos productos."
            },
            {
                "id_naturaleza": "0",
                "nombre_naturaleza": "Privado o Mixta",
                "id_sector": "3",
                "nombre_sector": "Transporte",
                "id_grupo": "0",
                "grupo_nombre": "--",
                "tooltip_sector": "Hace parte del sector terciario, e incluye transporte de carga, servicio de transporte público, transporte terrestre, aéreo, marítimo, etc."
            },
            {
                "id_naturaleza": "0",
                "nombre_naturaleza": "Privado o Mixta",
                "id_sector": "4",
                "nombre_sector": "Comercio",
                "id_grupo": "0",
                "grupo_nombre": "--",
                "tooltip_sector": "Hace parte del sector terciario de la economía, e incluye comercio al por mayor, minorista, centros comerciales, cámaras de comercio, San Andresitos, plazas de mercado y, en general, a todos aquellos que se relacionan con la actividad de comercio de diversos productos a nivel nacional o internacional."
            },
            {
                "id_naturaleza": "0",
                "nombre_naturaleza": "Privado o Mixta",
                "id_sector": "5",
                "nombre_sector": "Financiero",
                "id_grupo": "0",
                "grupo_nombre": "--",
                "tooltip_sector": "En este sector se incluyen todas aquellas organizaciones relacionadas con actividades bancarias y financieras, aseguradoras, fondos de pensiones y cesantías, fiduciarias, etc."
            },
            {
                "id_naturaleza": "0",
                "nombre_naturaleza": "Privado o Mixta",
                "id_sector": "6",
                "nombre_sector": "Construcción",
                "id_grupo": "0",
                "grupo_nombre": "--",
                "tooltip_sector": "En este sector se incluyen las empresas y organizaciones relacionadas con la construcción, al igual que los arquitectos e ingenieros, las empresas productoras de materiales para la construcción, etc."
            },
            {
                "id_naturaleza": "0",
                "nombre_naturaleza": "Privado o Mixta",
                "id_sector": "7",
                "nombre_sector": "Minero y energético",
                "id_grupo": "0",
                "grupo_nombre": "--",
                "tooltip_sector": "Se incluyen en él todas las empresas que se relacionan con la actividad minera y energética de cualquier tipo (extracción de carbón, esmeraldas, gas y petróleo; empresas generadoras de energía; etc.)."
            },
            {
                "id_naturaleza": "0",
                "nombre_naturaleza": "Privado o Mixta",
                "id_sector": "8",
                "nombre_sector": "Solidario",
                "id_grupo": "0",
                "grupo_nombre": "--",
                "tooltip_sector": "En este sector se incluyen las cooperativas, las cajas de compensación familiar, las empresas solidarias de salud, asociaciones, entre otras."
            },
            {
                "id_naturaleza": "0",
                "nombre_naturaleza": "Privado o Mixta",
                "id_sector": "9",
                "nombre_sector": "Comunicaciones",
                "id_grupo": "0",
                "grupo_nombre": "--",
                "tooltip_sector": "En este sector se incluyen todas las empresas y organizaciones relacionadas con los medios de comunicación como (telefonía fija y celular, empresas de publicidad, periódicos, editoriales, etc.)."
            },
            {
                "id_naturaleza": "1",
                "nombre_naturaleza": "Público",
                "id_sector": "10",
                "nombre_sector": "Rama Legislativa",
                "id_grupo": "0",
                "grupo_nombre": "Senado de la República",
                "tooltip_sector": "--"
            },
            {
                "id_naturaleza": "1",
                "nombre_naturaleza": "Público",
                "id_sector": "10",
                "nombre_sector": "Rama Legislativa",
                "id_grupo": "1",
                "grupo_nombre": "Cámara de Representantes",
                "tooltip_sector": "--"
            },
            {
                "id_naturaleza": "1",
                "nombre_naturaleza": "Público",
                "id_sector": "0",
                "nombre_sector": "Rama Ejecutiva",
                "id_grupo": "0",
                "grupo_nombre": "Sector del Interior",
                "tooltip_sector": "--"
            },
            {
                "id_naturaleza": "1",
                "nombre_naturaleza": "Público",
                "id_sector": "0",
                "nombre_sector": "Rama Ejecutiva",
                "id_grupo": "1",
                "grupo_nombre": "Sector Relaciones Exteriores",
                "tooltip_sector": "--"
            },
            {
                "id_naturaleza": "1",
                "nombre_naturaleza": "Público",
                "id_sector": "0",
                "nombre_sector": "Rama Ejecutiva",
                "id_grupo": "2",
                "grupo_nombre": "Sector Hacienda y Crédito Público",
                "tooltip_sector": "--"
            },
            {
                "id_naturaleza": "1",
                "nombre_naturaleza": "Público",
                "id_sector": "0",
                "nombre_sector": "Rama Ejecutiva",
                "id_grupo": "3",
                "grupo_nombre": "Sector Justicia y del Derecho",
                "tooltip_sector": "--"
            },
            {
                "id_naturaleza": "1",
                "nombre_naturaleza": "Público",
                "id_sector": "0",
                "nombre_sector": "Rama Ejecutiva",
                "id_grupo": "4",
                "grupo_nombre": "Sector de la Defensa Nacional",
                "tooltip_sector": "--"
            },
            {
                "id_naturaleza": "1",
                "nombre_naturaleza": "Público",
                "id_sector": "0",
                "nombre_sector": "Rama Ejecutiva",
                "id_grupo": "5",
                "grupo_nombre": "Sector Agropecuario, Pesquero y de Desarrollo Rural",
                "tooltip_sector": "--"
            },
            {
                "id_naturaleza": "1",
                "nombre_naturaleza": "Público",
                "id_sector": "0",
                "nombre_sector": "Rama Ejecutiva",
                "id_grupo": "6",
                "grupo_nombre": "Sector Salud y de la Protección Social",
                "tooltip_sector": "--"
            },
            {
                "id_naturaleza": "1",
                "nombre_naturaleza": "Público",
                "id_sector": "0",
                "nombre_sector": "Rama Ejecutiva",
                "id_grupo": "7",
                "grupo_nombre": "Sector del Trabajo",
                "tooltip_sector": "--"
            },
            {
                "id_naturaleza": "1",
                "nombre_naturaleza": "Público",
                "id_sector": "0",
                "nombre_sector": "Rama Ejecutiva",
                "id_grupo": "8",
                "grupo_nombre": "Sector Minas y Energía",
                "tooltip_sector": "--"
            },
            {
                "id_naturaleza": "1",
                "nombre_naturaleza": "Público",
                "id_sector": "0",
                "nombre_sector": "Rama Ejecutiva",
                "id_grupo": "9",
                "grupo_nombre": "Sector de Comercio, Industria y Turismo",
                "tooltip_sector": "--"
            },
            {
                "id_naturaleza": "1",
                "nombre_naturaleza": "Público",
                "id_sector": "0",
                "nombre_sector": "Rama Ejecutiva",
                "id_grupo": "10",
                "grupo_nombre": "Sector Educación Nacional",
                "tooltip_sector": "--"
            },
            {
                "id_naturaleza": "1",
                "nombre_naturaleza": "Público",
                "id_sector": "0",
                "nombre_sector": "Rama Ejecutiva",
                "id_grupo": "11",
                "grupo_nombre": "Sector Ambiente y Desarrollo Sostenible",
                "tooltip_sector": "--"
            },
            {
                "id_naturaleza": "1",
                "nombre_naturaleza": "Público",
                "id_sector": "0",
                "nombre_sector": "Rama Ejecutiva",
                "id_grupo": "12",
                "grupo_nombre": "Sector Vivienda, Ciudad y Territorio",
                "tooltip_sector": "--"
            },
            {
                "id_naturaleza": "1",
                "nombre_naturaleza": "Público",
                "id_sector": "0",
                "nombre_sector": "Rama Ejecutiva",
                "id_grupo": "13",
                "grupo_nombre": "Sector de las Tecnologías de la Información y las Comunicaciones",
                "tooltip_sector": "--"
            },
            {
                "id_naturaleza": "1",
                "nombre_naturaleza": "Público",
                "id_sector": "0",
                "nombre_sector": "Rama Ejecutiva",
                "id_grupo": "14",
                "grupo_nombre": "Sector Transporte",
                "tooltip_sector": "--"
            },
            {
                "id_naturaleza": "1",
                "nombre_naturaleza": "Público",
                "id_sector": "0",
                "nombre_sector": "Rama Ejecutiva",
                "id_grupo": "15",
                "grupo_nombre": "Sector Cultura",
                "tooltip_sector": "--"
            },
            {
                "id_naturaleza": "1",
                "nombre_naturaleza": "Público",
                "id_sector": "0",
                "nombre_sector": "Rama Ejecutiva",
                "id_grupo": "16",
                "grupo_nombre": "Sector Presidencia de la República",
                "tooltip_sector": "--"
            },
            {
                "id_naturaleza": "1",
                "nombre_naturaleza": "Público",
                "id_sector": "0",
                "nombre_sector": "Rama Ejecutiva",
                "id_grupo": "17",
                "grupo_nombre": "Sector de Planeación",
                "tooltip_sector": "--"
            },
            {
                "id_naturaleza": "1",
                "nombre_naturaleza": "Público",
                "id_sector": "0",
                "nombre_sector": "Rama Ejecutiva",
                "id_grupo": "18",
                "grupo_nombre": "Sector Función Pública",
                "tooltip_sector": "--"
            },
            {
                "id_naturaleza": "1",
                "nombre_naturaleza": "Público",
                "id_sector": "0",
                "nombre_sector": "Rama Ejecutiva",
                "id_grupo": "19",
                "grupo_nombre": "Sector Inclusión Social y Reconciliación",
                "tooltip_sector": "--"
            },
            {
                "id_naturaleza": "1",
                "nombre_naturaleza": "Público",
                "id_sector": "0",
                "nombre_sector": "Rama Ejecutiva",
                "id_grupo": "20",
                "grupo_nombre": "Sector Inteligencia Estratégica y Contrainteligencia",
                "tooltip_sector": "--"
            },
            {
                "id_naturaleza": "1",
                "nombre_naturaleza": "Público",
                "id_sector": "0",
                "nombre_sector": "Rama Ejecutiva",
                "id_grupo": "21",
                "grupo_nombre": "Sector Información Estadística",
                "tooltip_sector": "--"
            },
            {
                "id_naturaleza": "1",
                "nombre_naturaleza": "Público",
                "id_sector": "0",
                "nombre_sector": "Rama Ejecutiva",
                "id_grupo": "22",
                "grupo_nombre": "Sector Administrativo del Deporte",
                "tooltip_sector": "--"
            },
            {
                "id_naturaleza": "1",
                "nombre_naturaleza": "Público",
                "id_sector": "0",
                "nombre_sector": "Rama Ejecutiva",
                "id_grupo": "23",
                "grupo_nombre": "Sector Ciencia y Tecnología",
                "tooltip_sector": "--"
            },
            {
                "id_naturaleza": "1",
                "nombre_naturaleza": "Público",
                "id_sector": "1",
                "nombre_sector": "Rama Judicial",
                "id_grupo": "0",
                "grupo_nombre": "Corte Constitucional",
                "tooltip_sector": "--"
            },
            {
                "id_naturaleza": "1",
                "nombre_naturaleza": "Público",
                "id_sector": "1",
                "nombre_sector": "Rama Judicial",
                "id_grupo": "1",
                "grupo_nombre": "Corte Suprema de Justicia",
                "tooltip_sector": "--"
            },
            {
                "id_naturaleza": "1",
                "nombre_naturaleza": "Público",
                "id_sector": "1",
                "nombre_sector": "Rama Judicial",
                "id_grupo": "2",
                "grupo_nombre": "Tribunales Superiores de Distrito Judicial",
                "tooltip_sector": "--"
            },
            {
                "id_naturaleza": "1",
                "nombre_naturaleza": "Público",
                "id_sector": "1",
                "nombre_sector": "Rama Judicial",
                "id_grupo": "3",
                "grupo_nombre": "Juzgados (Civiles, laborales, penales, de familia,promiscuos, de pequeñas causas)",
                "tooltip_sector": "--"
            },
            {
                "id_naturaleza": "1",
                "nombre_naturaleza": "Público",
                "id_sector": "1",
                "nombre_sector": "Rama Judicial",
                "id_grupo": "4",
                "grupo_nombre": "Consejo de Estado",
                "tooltip_sector": "--"
            },
            {
                "id_naturaleza": "1",
                "nombre_naturaleza": "Público",
                "id_sector": "1",
                "nombre_sector": "Rama Judicial",
                "id_grupo": "5",
                "grupo_nombre": "Tribunales Administrativos",
                "tooltip_sector": "--"
            },
            {
                "id_naturaleza": "1",
                "nombre_naturaleza": "Público",
                "id_sector": "1",
                "nombre_sector": "Rama Judicial",
                "id_grupo": "6",
                "grupo_nombre": "Juzgados Administrativos",
                "tooltip_sector": "--"
            },
            {
                "id_naturaleza": "1",
                "nombre_naturaleza": "Público",
                "id_sector": "1",
                "nombre_sector": "Rama Judicial",
                "id_grupo": "7",
                "grupo_nombre": "Jurisdicciones Especiales - Jueces de Paz",
                "tooltip_sector": "--"
            },
            {
                "id_naturaleza": "1",
                "nombre_naturaleza": "Público",
                "id_sector": "1",
                "nombre_sector": "Rama Judicial",
                "id_grupo": "8",
                "grupo_nombre": "Jurisdicciones Especiales - Jurisdicción Comunidades Indígenas",
                "tooltip_sector": "--"
            },
            {
                "id_naturaleza": "1",
                "nombre_naturaleza": "Público",
                "id_sector": "1",
                "nombre_sector": "Rama Judicial",
                "id_grupo": "9",
                "grupo_nombre": "Consejo Superior de la Judicatura",
                "tooltip_sector": "--"
            },
            {
                "id_naturaleza": "1",
                "nombre_naturaleza": "Público",
                "id_sector": "1",
                "nombre_sector": "Rama Judicial",
                "id_grupo": "10",
                "grupo_nombre": "Dirección Ejecutiva de Administración Judicial",
                "tooltip_sector": "--"
            },
            {
                "id_naturaleza": "1",
                "nombre_naturaleza": "Público",
                "id_sector": "1",
                "nombre_sector": "Rama Judicial",
                "id_grupo": "11",
                "grupo_nombre": "Consejos Seccionales de la Judicatura",
                "tooltip_sector": "--"
            },
            {
                "id_naturaleza": "1",
                "nombre_naturaleza": "Público",
                "id_sector": "1",
                "nombre_sector": "Rama Judicial",
                "id_grupo": "12",
                "grupo_nombre": "Fiscalía General de la Nación",
                "tooltip_sector": "--"
            },
            {
                "id_naturaleza": "1",
                "nombre_naturaleza": "Público",
                "id_sector": "1",
                "nombre_sector": "Rama Judicial",
                "id_grupo": "13",
                "grupo_nombre": "Instituto de Medicina Legal y Ciencias Forenses",
                "tooltip_sector": "--"
            },
            {
                "id_naturaleza": "1",
                "nombre_naturaleza": "Público",
                "id_sector": "1",
                "nombre_sector": "Rama Judicial",
                "id_grupo": "14",
                "grupo_nombre": "Institución Universitaria Conocimiento e Innovación para la Justicia (CIJ)",
                "tooltip_sector": "--"
            },
            {
                "id_naturaleza": "1",
                "nombre_naturaleza": "Público",
                "id_sector": "2",
                "nombre_sector": "Órganos Autónomos",
                "id_grupo": "0",
                "grupo_nombre": "Banco de la República",
                "tooltip_sector": "--"
            },
            {
                "id_naturaleza": "1",
                "nombre_naturaleza": "Público",
                "id_sector": "2",
                "nombre_sector": "Órganos Autónomos",
                "id_grupo": "1",
                "grupo_nombre": "Comisión Nacional del Servicio Civil - CNSC",
                "tooltip_sector": "--"
            },
            {
                "id_naturaleza": "1",
                "nombre_naturaleza": "Público",
                "id_sector": "2",
                "nombre_sector": "Órganos Autónomos",
                "id_grupo": "2",
                "grupo_nombre": "Corporaciones Autónomas",
                "tooltip_sector": "--"
            },
            {
                "id_naturaleza": "1",
                "nombre_naturaleza": "Público",
                "id_sector": "2",
                "nombre_sector": "Órganos Autónomos",
                "id_grupo": "3",
                "grupo_nombre": "Entes Universitarios Autónomos",
                "tooltip_sector": "--"
            },
            {
                "id_naturaleza": "1",
                "nombre_naturaleza": "Público",
                "id_sector": "2",
                "nombre_sector": "Órganos Autónomos",
                "id_grupo": "4",
                "grupo_nombre": "Autoridad Nacional de Televisión - ANTV",
                "tooltip_sector": "--"
            },
            {
                "id_naturaleza": "1",
                "nombre_naturaleza": "Público",
                "id_sector": "3",
                "nombre_sector": "Organización Electoral",
                "id_grupo": "0",
                "grupo_nombre": "Registraduría Nacional del Estado Civil",
                "tooltip_sector": "--"
            },
            {
                "id_naturaleza": "1",
                "nombre_naturaleza": "Público",
                "id_sector": "3",
                "nombre_sector": "Organización Electoral",
                "id_grupo": "1",
                "grupo_nombre": "Consejo Nacional Electoral",
                "tooltip_sector": "--"
            },
            {
                "id_naturaleza": "1",
                "nombre_naturaleza": "Público",
                "id_sector": "4",
                "nombre_sector": "Organismos de Control",
                "id_grupo": "0",
                "grupo_nombre": "Ministerio Público - Procuraduría General de la Nación",
                "tooltip_sector": "--"
            },
            {
                "id_naturaleza": "1",
                "nombre_naturaleza": "Público",
                "id_sector": "4",
                "nombre_sector": "Organismos de Control",
                "id_grupo": "1",
                "grupo_nombre": "Ministerio Público - Defensoría del Pueblo",
                "tooltip_sector": "--"
            },
            {
                "id_naturaleza": "1",
                "nombre_naturaleza": "Público",
                "id_sector": "4",
                "nombre_sector": "Organismos de Control",
                "id_grupo": "2",
                "grupo_nombre": "Ministerio Público - Personerías Distritales y Municipales",
                "tooltip_sector": "--"
            },
            {
                "id_naturaleza": "1",
                "nombre_naturaleza": "Público",
                "id_sector": "4",
                "nombre_sector": "Organismos de Control",
                "id_grupo": "3",
                "grupo_nombre": "Contraloría General de la República",
                "tooltip_sector": "--"
            },
            {
                "id_naturaleza": "1",
                "nombre_naturaleza": "Público",
                "id_sector": "4",
                "nombre_sector": "Organismos de Control",
                "id_grupo": "4",
                "grupo_nombre": "Contralorías Territoriales",
                "tooltip_sector": "--"
            },
            {
                "id_naturaleza": "1",
                "nombre_naturaleza": "Público",
                "id_sector": "4",
                "nombre_sector": "Organismos de Control",
                "id_grupo": "5",
                "grupo_nombre": "Auditoria General de la República",
                "tooltip_sector": "--"
            },
            {
                "id_naturaleza": "1",
                "nombre_naturaleza": "Público",
                "id_sector": "5",
                "nombre_sector": "Sistema Integral de Verdad, Justicia, Reparación y No Repetición",
                "id_grupo": "0",
                "grupo_nombre": "Comisión para el Esclarecimiento de la Verdad, la Convivencia y la No Repetición Civil",
                "tooltip_sector": "--"
            },
            {
                "id_naturaleza": "1",
                "nombre_naturaleza": "Público",
                "id_sector": "5",
                "nombre_sector": "Sistema Integral de Verdad, Justicia, Reparación y No Repetición",
                "id_grupo": "1",
                "grupo_nombre": "Unidad de Búsqueda de Personas Dadas por Desaparecidos en el Contexto y en Razón del Conflicto Armado",
                "tooltip_sector": "--"
            },
            {
                "id_naturaleza": "1",
                "nombre_naturaleza": "Público",
                "id_sector": "5",
                "nombre_sector": "Sistema Integral de Verdad, Justicia, Reparación y No Repetición",
                "id_grupo": "2",
                "grupo_nombre": "Jurisdicción Especial para la Paz",
                "tooltip_sector": "--"
            }
        ];
    }

    submitForm(data:any): Observable<any> {
        return this.httpClient.post<any>(this.creatRadUrl, data);
    }

    submitAnex(data:any): Observable<any> {
        return this.httpClient.post<any>(this.creatAnxUrl, data);
    }

    savePola(data:any) {
        this.arrPola = data;
    }

    getPola(){
        return this.arrPola;
    }

    dataSelect(): Promise <any> {
        return this.httpClient.post<any>(this.radUrl,  {
            diviPola : true,
            tipoDoc : true,
            genero: true,
            discapacidad: true,
            linguistica: true,
            estrato: true,
            infPoblacional: true,
            rangoEdad1: true,
            rangoEdad2: true,
            grupoEspecial: true,
            atenPreferencial: true,
            tipoDePeticion: true,
            tipoDeSolicitud: true
         })
        .toPromise()
        .then(dataSelect => {
            return dataSelect;
        });
    }

    httpError (error:any) {
        let msg = '';
        if(error.error instanceof ErrorEvent) {
            // client side error
            msg = error.error.message;
        } else {
            // server side error
            msg = `Error Code: ${error.status}\nMessage: ${error.message}`;
        }
        console.log(msg);
        return throwError(msg);
    }

}

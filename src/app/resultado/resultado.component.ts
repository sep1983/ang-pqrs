import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { environment } from '../../environments/environment'

@Component({
  selector: 'app-resultado',
  templateUrl: './resultado.component.html',
  styleUrls: ['./resultado.component.css']
})
export class ResultadoComponent implements OnInit {

  url:string = '';
  selectedId: number;
  verifica: string;


  constructor(private route: ActivatedRoute) {
    this.url = environment.base;
  }

  ngOnInit() {
    this.selectedId = Number(this.route.snapshot.paramMap.get('id'));
    this.verifica = this.route.snapshot.paramMap.get('verificacion');
  }

  OnClick() {
    console.log(this.url + '?'+ this.selectedId + '-' + this.verifica);
    window.location.href = this.url + '?cod='+ this.selectedId + '-' + this.verifica;
  }

}

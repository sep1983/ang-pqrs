import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { ResultadoComponent } from './resultado/resultado.component';
import { PqrsdComponent } from './pqrsd/pqrsd.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: PqrsdComponent
  },
  {
    path: 'fw',
    component: PqrsdComponent
  },
  {
    path: 'radicado/:id/:verificacion',
    component: ResultadoComponent
  },
  {
    path: 'pqrsd',
    component: PqrsdComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: []
})

export class AppRoutingModule { }

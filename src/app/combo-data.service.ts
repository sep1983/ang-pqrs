import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

export interface Divipola {
    id_continente: number;
    name_continente: string;
    id_pais: number;
    name_pais: string;
    id_departamento: number;
    name_departamento: string;
    id_municipio: number;
    name_municipio: string;
}

@Injectable({
  providedIn: 'root'
})

export class ComboDataService {

    private divUrl = 'http://localhost/argogpl/webService/consultaWeb.php';
    constructor(private httpClient: HttpClient) { }

    httpHeader = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json'
        })
    }


    getDiviPola(): Observable<Divipola>{
        return this.httpClient.get<Divipola>(this.divUrl + 'divpol=' + true).pipe(
            retry(1), catchError(this. httpError)
        );
    }

    httpError (error:any) {
        let msg = '';
        if(error.error instanceof ErrorEvent) {
            // client side error
            msg = error.error.message;
        } else {
            // server side error
            msg = `Error Code: ${error.status}\nMessage: ${error.message}`;
        }
        console.log(msg);
        return throwError(msg);
    }
}

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pqrsd',
  templateUrl: './pqrsd.component.html',
  styleUrls: ['./pqrsd.component.css']
})
export class PqrsdComponent implements OnInit {

    title = 'insor-pqrs';
    tabselector = 'personaltab';

    constructor() {};

    ngOnInit(): void {};

    buttonTitle:string = "Ver mas ...";

    visible:boolean = false;

    showhideUtility() {
        this.visible = this.visible?false:true;
        this.buttonTitle = this.visible? "Ocultar" : "Ver mas ...";
    }

}

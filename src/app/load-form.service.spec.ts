import { TestBed } from '@angular/core/testing';

import { LoadFormService } from './load-form.service';

describe('LoadFormService', () => {
  let service: LoadFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LoadFormService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

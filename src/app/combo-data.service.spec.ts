import { TestBed } from '@angular/core/testing';

import { ComboDataService } from './combo-data.service';

describe('ComboDataService', () => {
  let service: ComboDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ComboDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
